var frontendApp = angular.module('frontendApp', ['ui.router', 'ui.router.compat', 'productApp']);
frontendApp.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('root', {
            url: '/',
            views: {
                '': {
                    templateUrl: 'app/layouts/site/layout.html'
                },
                'header@root': {
                    templateUrl: 'app/layouts/site/header.html'
                },
                'content@root': {                    
                    templateUrl: 'app/component/site/views/index.html'
                },
                'footer@root': {
                    templateUrl: 'app/layouts/site/footer.html'
                },
            }
        })
        .state('root.home', {
            url: 'home',
            views: {
                'content@root': {
                    templateUrl: 'app/component/site/views/index.html'
                }
            }
        })
        .state('root.about', {
            url: 'about',
            views: {
                'content@root': {
                    templateUrl: 'app/component/site/views/about.html'
                }
            }
        })
        .state('root.contact', {
            url: 'contact',
            views: {
                'content@root': {
                    templateUrl: 'app/component/site/views/contact.html'
                }
            }
        })
        .state('root.login', {
            url: 'login',
            views: {
                'content@root': {
                    templateUrl: 'app/component/site/views/login.html'
                }
            }
        })
        .state('root.register', {
            url: 'register',
            views: {
                'content@root': {
                    templateUrl: 'app/component/site/views/register.html'
                }
            }
        })
        .state('root.forgotpassword', {
            url: 'forgot-password',
            views: {
                'content@root': {
                    templateUrl: 'app/component/site/views/forgotpassword.html'
                }
            }
        })
        .state('root.terms', {
            url: 'terms-conditions',
            views: {
                'content@root': {
                    templateUrl: 'app/component/site/views/terms.html'
                }
            }
        })
        .state('root.rightSidebar', {
            url: 'right-sidebar',
            views: {
                'content@root': {
                    templateUrl: 'app/component/site/views/sidebar-right.html'
                }
            }
        })
        .state('root.leftSidebar', {
            url: 'left-sidebar',
            views: {
                'content@root': {
                    templateUrl: 'app/component/site/views/sidebar-left.html'
                }
            }
        })
});
