'use strict';

productApp.controller('ProductController', function($scope, $http, $stateParams, $state, $rootScope, $location) {
    //retrieve employees listing from API
    $scope.productcategories = [];
    $scope.totalPages = 0;
    $scope.currentPage = 1;
    $scope.range = [];
    $scope.libraryTemp = {};
    $scope.totalProductTemp = {};
    $scope.totalProducts = 0;


   	
    $scope.getProductListing = function(pageNumber) {
        debugger;
        if(pageNumber !== 1) {
            if($.inArray( pageNumber,$scope.range) === -1) {
            return false;
            } 
        }

        if(pageNumber === undefined){
            pageNumber = '1';
        }
    	if(! $.isEmptyObject($scope.libraryTemp)){
	    $http.get($rootScope.API_URL + "product?search="+$scope.searchText+"&page="+pageNumber)
	            .success(function(response) {

	                $scope.products= response.data;
                    $scope.totalPages   = response.last_page;
                    $scope.currentPage  = response.current_page;
                    $scope.totalProducts = response.total;

                    // Pagination Range
                    var pages = [];

                    for(var i=1;i<=response.last_page;i++) {          
                        pages.push(i);
                    }

                    $scope.range = pages; 

	            });

        }
        else {

            $http.get($rootScope.API_URL + "product?page="+pageNumber)
                .success(function(response) {

                    $scope.products = response.data;
                    $scope.totalPages   = response.last_page;
                    $scope.currentPage  = response.current_page;
                    $scope.totalProducts = response.total;
                    // Pagination Range
                    var pages = [];

                    for(var i=1;i<=response.last_page;i++) {          
                        pages.push(i);
                    }

                    $scope.range = pages; 

            });

        }
    }

    $scope.searchDB = function(){
      debugger;
      if($scope.searchText.length >= 3){
          if($.isEmptyObject($scope.libraryTemp)){
              $scope.libraryTemp = $scope.products;
              $scope.totalProductTemp = $scope.totalProducts;
              $scope.products= {};
          }
          $scope.getProductListing(1);
      }else{
          if(! $.isEmptyObject($scope.libraryTemp)){
              $scope.products = $scope.libraryTemp ;
              $scope.totalProductTemp = $scope.totalProductsTemp;
              $scope.libraryTemp = {};
          }
          $scope.getProductListing(1);
      }
    }
    
    //save new record / update existing record
    $scope.save = function(state, id, currentPage) {
    	debugger;
        var url = $rootScope.API_URL + "addproduct";
        
        if (state === 'edit'){
            url = $rootScope.API_URL + "updateproduct/"+id;
        }
        
        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.product),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response) {
            //$location.path('administrator/categories',{pageNumber:$scope.currentPage});
    		$state.go('administrator.product',{pageNumber:currentPage});
        }).error(function(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check the log for details');
        });
    }

    $scope.edit = function() {
    	debugger;
    	var id = $stateParams.id;
        $scope.currentPage = $stateParams.currentPage;

    	$http.get($rootScope.API_URL + "editproduct/"+id)
	            .success(function(response) {
	                $scope.product = response;
	            });

    }


    $scope.view = function() {

        var id = $stateParams.id;
        $http.get($rootScope.API_URL + "viewproduct/"+id)
                .success(function(response) {
                    $scope.product = response;
                });

    }


    $scope.changeStatus = function(id,status) {
   		var isConfirmDelete = confirm('Are you sure you want change the status?');
        if (isConfirmDelete) {
   		 	var dataobj = {
	   		 	id : id,
	   		 	status: status
	   		 };

	   		$http({
	            method: 'POST',
	            url: $rootScope.API_URL + "changestatus",
	            data: dataobj            
	        }).success(function(response) {
	    		$scope.getCategoryListing($scope.currentPage);
	        }).error(function(response) {
	            console.log(response);
	            alert('This is embarassing. An error has occured. Please check the log for details');
	        });
    	}

    	
    }

    //delete record
    $scope.confirmDelete = function(id) {
        var isConfirmDelete = confirm('Are you sure you want this record?');
        if (isConfirmDelete) {
            $http({
                method: 'DELETE',
                url: $rootScope.API_URL + 'product/' + id
            }).success(function(data) {
                        
                        $scope.getProductListing(1);
                        
                    }).
                    error(function(data) {
                        console.log(data);
                        alert('Unable to delete');
                    });
        } else {
            return false;
        }
    }




});



