var productApp = angular.module('productApp', ['ui.router', 'ui.router.compat']);
productApp.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('administrator.product', {
            url: '/products',
            views: {
                'content@administrator': {
                    templateUrl: 'app/component/admin/products/views/index.html'
                }
            }
        })
        .state('administrator.product.create', {
            url: '/create',
            views: {
                'content@administrator': {
                    templateUrl: 'app/component/admin/products/views/create.html'
                }
            }
        })
        .state('administrator.product.edit', {
            url: '/edit/:id',
            views: {
                'content@administrator': {
                    templateUrl: 'app/component/admin/products/views/edit.html'
                }
            }
        })
        .state('administrator.product.view', {
            url: '/:id',
            views: {
                'content@administrator': {
                    templateUrl: 'app/component/admin/products/views/view.html'
                }
            }
        })
});

productApp.controller('ProductController', function($scope, $state, $stateParams) {

    $scope.productID = $stateParams.id;

});
