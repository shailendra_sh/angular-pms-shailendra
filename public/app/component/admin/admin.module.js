var administratorApp = angular.module('administratorApp', ['ui.router', 'ui.router.compat', 'productApp', 'categoryApp', 'userApp']);
administratorApp.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('administrator', {
            url: '/administrator',
            views: {
                '': {
                    templateUrl: 'app/layouts/admin/layout.html'
                },
                'header@administrator': {
                    templateUrl: 'app/layouts/admin/header.html'
                },
                'sidebarLeft@administrator': {
                    templateUrl: 'app/layouts/admin/sidebar-left.html'
                },
                'sidebarRight@administrator': {
                    templateUrl: 'app/layouts/admin/sidebar-right.html'
                },
                'content@administrator': {
                    templateUrl: 'app/component/admin/dashboard/index.html'
                },
                'footer@administrator': {
                    templateUrl: 'app/layouts/admin/footer.html'
                }
            }
        })
        .state('administrator.dashboard', {
            url: '/dashboard',
            views: {
                'content@administrator': {
                    templateUrl: 'app/component/admin/dashboard/index.html'
                }
            }
        })
        .state('administratorAuth', {
            url: '/administrator/auth',
            views: {
                '': {
                    templateUrl: 'app/layouts/admin/auth/layout.html'
                },
                'content@administratorAuth': {
                    templateUrl: 'app/component/admin/auth/login.html'
                }
            }
        })
        .state('administratorAuth.login', {
            url: '/login',
            views: {
                '': {
                    templateUrl: 'app/layouts/admin/auth/layout.html'
                },
                'content@administratorAuth': {
                    templateUrl: 'app/component/admin/auth/login.html'
                }
            }
        })
        .state('administratorAuth.register', {
            url: '/register',
            views: {
                'content@administratorAuth': {
                    templateUrl: 'app/component/admin/auth/register.html'
                }
            }
        })
        .state('administratorAuth.forgotpassword', {
            url: '/forgot-password',
            views: {
                'content@administratorAuth': {
                    templateUrl: 'app/component/admin/auth/forgotpassword.html'
                }
            }
        })
});
