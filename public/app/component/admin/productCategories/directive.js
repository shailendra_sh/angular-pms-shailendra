categoryApp.directive('postsPagination', function(){ 
  
   return{ 
      restrict: 'E',
      template: '<ul class="pagination pull-right">'+
        '<li ng-class="{disabled:currentPage === 1}"><a href="javascript:void(0)" ng-click="getCategoryListing(1)">&laquo;</a></li>'+
        '<li ng-class="{disabled:currentPage === 1}"><a href="javascript:void(0)" ng-click="getCategoryListing(currentPage-1)">&lsaquo; Prev</a></li>'+
        '<li ng-repeat="i in range" ng-class="{active : currentPage == i}">'+
            '<a href="javascript:void(0)" ng-click="getCategoryListing(i)">{{i}}</a>'+
        '</li>'+
        '<li ng-class="{disabled:currentPage === totalPages}"><a href="javascript:void(0)" ng-click="getCategoryListing(currentPage+1)">Next &rsaquo;</a></li>'+
        '<li ng-class="{disabled:currentPage === totalPages}"><a href="javascript:void(0)" ng-click="getCategoryListing(totalPages)">&raquo;</a></li>'+
      '</ul>'
   };
});

