'use strict';

categoryApp.controller('ProductCategoryController', function($scope, $http, $stateParams, $state, $rootScope, $location) {
    //retrieve employees listing from API
    $scope.productcategories = [];
    $scope.totalPages = 0;
    $scope.currentPage = 1;
    $scope.range = [];
    $scope.libraryTemp = {};
    $scope.totalCategoryTemp = {};
    $scope.totalCategories = 0;


   	
    $scope.getCategoryListing = function(pageNumber) {

        if(pageNumber !== 1) {
            if($.inArray( pageNumber,$scope.range) === -1) {
            return false;
            } 
        }

        if(pageNumber === undefined){
            pageNumber = '1';
        }
    	if(! $.isEmptyObject($scope.libraryTemp)){
	    $http.get($rootScope.API_URL + "category?search="+$scope.searchText+"&page="+pageNumber)
	            .success(function(response) {

	                $scope.productcategories = response.data;
                    $scope.totalPages   = response.last_page;
                    $scope.currentPage  = response.current_page;
                    $scope.totalCategories = response.total;

                    // Pagination Range
                    var pages = [];

                    for(var i=1;i<=response.last_page;i++) {          
                        pages.push(i);
                    }

                    $scope.range = pages; 

	            });

        }
        else {

            $http.get($rootScope.API_URL + "category?page="+pageNumber)
                .success(function(response) {

                    $scope.productcategories = response.data;
                    $scope.totalPages   = response.last_page;
                    $scope.currentPage  = response.current_page;
                    $scope.totalCategories = response.total;
                    // Pagination Range
                    var pages = [];

                    for(var i=1;i<=response.last_page;i++) {          
                        pages.push(i);
                    }

                    $scope.range = pages; 

            });

        }
    }

    $scope.searchDB = function(){

      if($scope.searchText.length >= 3){
          if($.isEmptyObject($scope.libraryTemp)){
              $scope.libraryTemp = $scope.productcategories;
              $scope.totalCategoryTemp = $scope.totalProducts;
              $scope.productcategories = {};
          }
          $scope.getCategoryListing(1);
      }else{
          if(! $.isEmptyObject($scope.libraryTemp)){
              $scope.productcategories = $scope.libraryTemp ;
              $scope.totalCategoryTemp = $scope.totalProductsTemp;
              $scope.libraryTemp = {};
          }
          $scope.getCategoryListing(1);
      }
    }
    
    //save new record / update existing record
    $scope.save = function(state, id, currentPage) {
    	
        var url = $rootScope.API_URL + "addcategory";
        
        if (state === 'edit'){
            url = $rootScope.API_URL + "updatecategory/"+id;
        }
        
        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.category),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response) {
            //$location.path('administrator/categories',{pageNumber:$scope.currentPage});
    		$state.go('administrator.category',{pageNumber:currentPage});
        }).error(function(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check the log for details');
        });
    }

    $scope.edit = function() {
    	
    	var id = $stateParams.id;
        $scope.currentPage = $stateParams.currentPage;

    	$http.get($rootScope.API_URL + "editcategory/"+id)
	            .success(function(response) {
	                $scope.category = response;
	            });

    }


    $scope.view = function() {

        var id = $stateParams.id;
        $http.get($rootScope.API_URL + "viewcategory/"+id)
                .success(function(response) {
                    $scope.category = response;
                });

    }


    $scope.changeStatus = function(id,status) {
   		var isConfirmDelete = confirm('Are you sure you want change the status?');
        if (isConfirmDelete) {
   		 	var dataobj = {
	   		 	id : id,
	   		 	status: status
	   		 };

	   		$http({
	            method: 'POST',
	            url: $rootScope.API_URL + "changestatus",
	            data: dataobj            
	        }).success(function(response) {
	    		$scope.getCategoryListing($scope.currentPage);
	        }).error(function(response) {
	            console.log(response);
	            alert('This is embarassing. An error has occured. Please check the log for details');
	        });
    	}

    	
    }

    //delete record
    $scope.confirmDelete = function(id) {
        var isConfirmDelete = confirm('Are you sure you want this record?');
        if (isConfirmDelete) {
            $http({
                method: 'DELETE',
                url: $rootScope.API_URL + 'category/' + id
            }).success(function(data) {
                        
                        $scope.getCategoryListing(1);
                        
                    }).
                    error(function(data) {
                        console.log(data);
                        alert('Unable to delete');
                    });
        } else {
            return false;
        }
    }




});



