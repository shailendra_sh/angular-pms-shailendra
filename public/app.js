var app = angular.module('myApp', ['ui.router', 'ui.router.compat', 'frontendApp', 'administratorApp','categoryApp' , 'ngResource']);

app.run(function($rootScope) {
    $rootScope.API_URL = 'http://localhost/html/angular-pms-shailendra/public/';
});

app.config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/home');
});
