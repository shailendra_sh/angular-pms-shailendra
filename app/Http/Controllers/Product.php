<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Products;
use Response;

class Product extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {

        $product = Products::orderBy('id', 'desc');
        if($request->get('search')){
            $product = $product->where("title", "LIKE", "%{$request->get('search')}%");
        }
        $product = $product->paginate(5);
        return Response::json($product);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $product = new Products;
        $product->title = $request->input('title');
        $product->description = $request->input('description');
        $product->product_category_id ='1';
        //$ProductCategory->image_name = $request->input('cat_image');
        $product->save();
        return 'Product Successfully Added';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Products::find($id);
        return $product;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $product = Products::find($id);
        return $product;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //echo $id;
        //die;
        $product = Products::find($id);
        $product->title = $request->input('title');
        $product->description = $request->input('description');
        $product->save();
        return "Sucess updating user #" . $product->id;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id) {
        $products = Products::find($id);

        $products->delete();

        return "Products record successfully deleted #" . $id;
    }

}
