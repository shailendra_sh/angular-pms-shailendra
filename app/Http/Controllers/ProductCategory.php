<?php

namespace App\Http\Controllers;

use Response;
use App\ProductCategories;
use Illuminate\Http\Request;
use App\Http\Requests;

class ProductCategory extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {			$category = ProductCategories::orderBy('id', 'desc');
    			if($request->get('search')){
            $category = $category->where("title", "LIKE", "%{$request->get('search')}%");
        	}
        	$category = $category->paginate(5);
        	return Response::json($category);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         	$ProductCategory = new ProductCategories;
	        $ProductCategory->title = $request->input('title');
	        $ProductCategory->description = $request->input('description');
	        $ProductCategory->save();
	        return 'Category Successfully Created';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       	$category = ProductCategories::find($id);
        return $category;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = ProductCategories::find($id);
        return $category;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = ProductCategories::find($id);
        $category->title = $request->input('title');
        $category->description = $request->input('description');
        $category->save();
        return "Sucess updating user #" . $category->id;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $category = ProductCategories::find($id);

        $category->delete();

        return "Category record successfully deleted #" . $id;
    }

    public function changestatus(Request $request)
    {		
    		if($request->input('status') == 0) {
    			$status = 1;
    		}
    		else {
    			$status = 0;
    		}
    		$category = ProductCategories::find($request->input('id'));
        $category->is_enabled = $status;
        $category->save();
        return "Sucess updating user #" . $category->id;
    }
}
