<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/category', 'ProductCategory@index');
Route::post('/addcategory', 'ProductCategory@store');
Route::get('/editcategory/{id}', 'ProductCategory@edit');
Route::post('/updatecategory/{id}', 'ProductCategory@update');
Route::get('/viewcategory/{id}', 'ProductCategory@show');
Route::delete('/category/{id}', 'ProductCategory@destroy');
Route::post('/changestatus', 'ProductCategory@changestatus');

Route::get('/product', 'Product@index');
Route::post('/addproduct', 'Product@store');
Route::get('/editproduct/{id}', 'Product@edit');
Route::post('/updateproduct/{id}', 'Product@update');
Route::delete('/product/{id}', 'Product@destroy');
Route::get('/viewproduct/{id}', 'Product@show');